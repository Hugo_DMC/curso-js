// Callbacks, funcion que se pasa como argumento a un metodo
let request = require('request');

request('https://www.google.ccom', function(){
  console.log("Termine la peticion");
});

console.log("Yo termine despues");

/*
Promesas, objeto que puede producir un objeto en el futuro, tienen distintos estados:
fullfiled: O compleatda, significa que la promesa se completo con exito
rejected: O rechazada, significa que la promesa no se completo con exito
pending: O pendiente, es el estado de la promesa cuando la operacion no ha terminado, aqui decimos que la promesa no se ha cumplido
settled: O finalizada, cuando la promesa termino ya sea con exito o con algun error
*/
let request = require('request-promise');

let promesa = request('https://www.google.com')
promesa.then(function(){ console.log("Termine la peticion")});
promesa.catch(function(err){console.log("Error!!")});

console.log("Yo termine despues");

/*
callbacks de la Promesa
then cuando la promesa se completo con exito fullfiled la promesa se ejecuto
catch rejected con problema o con error
finally independientemente si la promesa se completo con exito o fallo);
*/

let promesa = new Promise(function(resolve, reject){
    resolve(10);
    reject("Algo salio mal");
});

//crear promesas
let request = require('request');

function leerPagina(url){
    return new Promise(function(resolve, reject){
        request(url, function(error, response){
            if(error) return reject(error);

            resolve(response);
        });
    });
}

let promesa = leerPagina('https://www.google.com');
promesa.then(r => console.log("finalice")).catch(err => console.log(err));


// multiples promesas
// let p1 = new Promise((resolve, reject) => setTimeout(resolve, 100, "Hola mundo"));
let p1 = new Promise((resolve, reject) => setTimeout(reject, 100, "error en la primera promesa"));

let p2 = new Promise((resolve, reject) => setTimeout(resolve, 600, "Segundo hola mundo"));

function finalizado(){
    console.log("Finalizo");
}

// p1.then(function(r){
//     console.log(r);
//     p2.then(function(r2){
//         console.log(r2);
//         finalizado();
//     })
// })
//otra forma
Promise.all([p1,p2]).then(function(resultados){
    console.log(resultados);
    finalizo();
}).catch(err => console.log(err));


//encadenar Promesas
function primerPromesa(){
    return new Promise((resolve, reject) => setTimeout(resolve, 100, "error en la primera promesa"));
}

function segundaPromesa(){
    return new Promise((resolve, reject) => setTimeout(resolve, 600, "Segundo hola mundo"));
}

primerPromesa().then(segundaPromesa).then(function(r){ console.log(r)})
function finalizado1(){
    console.log("Finalizo");
}
