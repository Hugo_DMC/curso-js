/*
Arreglos:
    Pueden almacenar cualquier tipo de dato de primera clase, fucniones, objetos, numeros, cadenas, booleanos e incluso arreglos.
    Una de las propiedades de los arreglos es su longitud, la longitud es igual a la cantidad de elementos dentro del arreglo.
    Cada elemento dentro del arreglo ocupa una posicion y se accede a dicho elemento usando el nombre del arreglo mas la posicion de elemento.
    Las posiciones de los elementos inician a contar desde 0, de manera que el primer elemento del arreglo tiene como indice el 0.
    La ultima posicion del arreglo es igual a la longitud del arreglo, menos uno, ese menos uno responde al hecho de que la cuenta inicia en 0;
*/

let calificaciones = [10,9,9,8,8];

let arreglo = ["rails", 1, "Laravel", {}, [1,2,3]];
console.log(arreglo);

// agrega al final
arreglo.push(100);
console.log(arreglo);

// agrega al principio
arreglo.unshift(100);
console.log(arreglo);

// elimina el ultimo elemento
arreglo.pop(100);
console.log(arreglo);

// elimina el primer elemento
arreglo.shift(100);
console.log(arreglo);

let arregloNumeros = [1,2,3,4,5,6,7,8,9,10];

for (let i = 0; i < arregloNumeros.length; i++){
    console.log(arregloNumeros[i]);
}

// foreach
let lenguajes = ["RUBY", "PHGP", "JAVA", "C"];

lenguajes.forEach(function(lenguaje, indice, arreglo){
    console.log(lenguaje, indice, arreglo);
});

lenguajes.forEach(function(lenguaje, indice, arreglo){
    console.log(this);
}, "hola");

// Map, genera un nuevo arreglo, despues de ejecutar la modificacion

let aNumeros = [10,2,3,1,5,6,27,8,9,10];

let cuadrados = aNumeros.map(function(numero){
    return numero * numero;
});

console.log(cuadrados);

let nuevoArreglo = aNumeros.filter(function(numero){
    return numero % 2 === 0;
});
console.log(nuevoArreglo);

// reduce
aNumeros.reduce(function(acc, elemento){
    console.log(acc);
    return 1;
},1);

// buscar
// idexOf -> ===, retorna la posicion
// iinclude -> ===, retorna true o false
// find -> funcion prueba -> elemento buscado
// findIndex -> funcion prueba -> posicion
// some -> funcion prueba -> true o false

console.log(aNumeros.indexOf(27));

// si no se necesita conocer la posicion y si solo existe, true o false
// el segundo argumento es la posicion de inicio
console.log(aNumeros.includes(27,5));

// find -> funcion de prueba -> elemento encontrado
// find(elemento, posicion, arreglo)
let respuestaFind = aNumeros.find(function(elemento, posicion, arreglo){
    return elemento === 10;
});

console.log(respuestaFind);

//posicion del elemento
let respuestaFindIndex = aNumeros.findIndex(function(elemento, posicion, arreglo){
    return elemento === 10;
});

console.log(respuestaFindIndex);

// true o false
let respuestaSome = aNumeros.some(function(elemento, posicion, arreglo){
    return elemento === 10;
});

console.log(respuestaSome);


// spread syntax (expande) (...)
// Iterable, se puede separar

let prueba = [1,2,3];

console.log(prueba);
console.log(...prueba); // console.log(1,2,3);

let pruebaNombre = "HUGO";

console.log(pruebaNombre);
console.log(...pruebaNombre); // console.log(1,2,3);

// rest parameters (unifica) (...)
function demo1(...arr){
    console.log(arr);
}

demo1(10,2);
//arguments no es un arreglo es un objeto
function calificaciones1(nombre,...calificaciones){
    console.log(nombre, calificaciones);
}

calificaciones1("HUGO",10,6,8,9);
