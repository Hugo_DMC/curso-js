
class Curso{
    constructor(titulo){
        this.titulo = titulo;
    }

    inscribir(){
        console.log("Inscrito");
    }
}

let javascript = new Curso("Curso de JS");

console.log(javascript);
javascript.inscribir();


// propiedades
class Curso1{
    title = "Hola";
    inscribir(nombre){
        this.nombre = nombre;
    }
    // inscribir = (nombre) => this.nombre = nombre;
}

let c = new Curso1();
console.log(c.title);

let a = new Curso1();
a.inscribir ("Hugo");
console.log(a.nombre);


// El alcance de propiedades es publico por defecto, # para variables privadas
class Materia{
    #title = "Estructura de datos";
    bienvenida(){
        console.log("Bienvenidos al curso de: " + this.#title);
    }
}

let ed = new Materia();
console.log(ed.#title); // error de propiedad privada
ed.bienvenida();


// metodo constructor, solo debe tener un constructor
class Clase{
    constructor(titulo, color = "Yellow"){
        this.titulo = titulo;
        this.color = color;
        console.log(arguments);
    }
}


// Herencia
class Human{
    especie = "Humano";
    respirar(){console.log("Inhala")}
}

class Admin extends Human{}

let admin = new Admin();

console.log(admin.especie);
admin.respirar();

class Player{
    play(){ this.video.play(); }
    duration{ return this.video.duration / 100; }
}

class Vimeo extends Player{
    // play(){ this.video.play(); }
    // duration{ return this.video.duration / 100; }
    open { this.redirectToVimeo( this.video);}
}

class YouTube extends Player{
    // play(){ this.video.play(); }
    // duration{ return this.video.duration / 100; }
    open { this.redirectToYouTube( this.video);}
}

// sobreescribir
class User{
    constructor(name){
        this.name = name;
    }
    saludar(){ console.log("Hola usuario");}
}

class Administrador extends User{
    constructor(name){
        super(name);
    }
    saludar (){
        super.saludar();
        console.log("Hola soy adminstrador");
    }
}

let administrador = new Administrador("Hugo");
administrador.saludar();
console.log(administrador.name);


// Encapsulacion
class Usuario{
    get nombre(){ return "HUGO";}
    set nombre(nombre){
        if(typeof nombre !== "string") throw Error("No es una cadena")
        this._nombre = nombre;}
}

let user = new Usuario();
console.log(user.nombre); // getter
user.nombre = "JOMPY"; // setter


// metodos y propiedades estaticas, se pueden acceder a los metodos sin
// instanciar un objeto.
class Api{
    static ENDPOINT = "localhost:3000";
    static get(){ console.log("Soy un metodo estatico");}
}

Api.get();
console.log( Api.ENDPOINT);
