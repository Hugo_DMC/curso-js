// JavaScript Object Notation -> JSON
let materias = {
    nombre: "Introduccion a la programacion",
    horario: "10:00-11:30",
    dias: "l,m,m,j",
    incribir: function(){
        console.log("Inscrito");
    }
}

console.log(materias.nombre);
console.log(materias["nombre"]);

materias.incribir();
materias["inscribir"] = function(){console.log("Inscrito v2");}
materias.incribir();


// Shorthand syntax, declarar funciones y propiedades
let nombre = "Hugo";
// let usuario = {nombre:nombre}
let usuario = {
    nombre,
    saludar(){console.log("Hola mundo cruel");}
} //ems 2015 con el identificador
console.log(usuario.nombre);
usuario.saludar();


// Duplicar o combinar objetos
//las propiedades repetidas asigan la ultima
let user = {
    edad: 29,
    nombre: "Hugo"
}

let esquemaPermisos = {
    nivel:2
}

let admin = {...user, ...esquemaPermisos, permisos: true};

console.log(admin);

// {}, targets o fuente, diferencia con los setters, si los utiliza
let copia = Object.assign({}, user);
console.log(copia);

let copia1 = Object.assign(user, esquemaPermisos);
console.log(copia1);


// Asignar por Destructuring assignment
let {nombre: username, apellido = "Hernandez"} = user;
console.log(username);
console.log(apellido);

let {nombre: username1, ...sobrante} = user;
console.log(username1);
console.log(sobrante);

function saludar2({nombre}){
    console.log(nombre);
}

saludar2(user);


// funciones constructoras

function hola(){
    console.log(this);
}

let objeto = new hola();

function Course(title){
    this.title = title;
    this.inscribir = function(){

    }
}

let objeto = new Course("Curso profesional de Ruby");
let objeto1 = new Course("Curso profesional de JS");
console.log(objeto);
console.log(objeto1);
