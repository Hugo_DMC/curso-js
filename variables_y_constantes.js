// let | var nombre = valor;
let nombre = "hugo";
console.log(nombre);

nombre = 3;
console.log(nombre);

nombre = 2.1;
console.log(nombre);

const PI = 3.1416;

/*
string String()
boolean Boolean()
numbers Number()
undefined
symbol Symbol()
null

BigInt()
los tipos primitvos son inmutables
*/
let number = "Hola";

console.log("20");

console.log(Number("20"));

console.log(parseInt("200"));
//Todos los objetos tienen el metodo toString()


// Type coercion
// conversion implicita cuando se usan 2 tipos de datos
console.log(10+"5")

// explicita cuando se obliga a un tipo de dato convertirse a otro
parseInt("50");


//Boolean true/false
let resultado = Boolean(1);
console.log(resultado);
//Cualquier cadena no vacia es true, los numeros cualquiera que no sea 0 es true
//Falso solo con estos: undefined, NaN, null, 0, -0, "", false


/*
Operadores de comparacion
    == Igual
    === estrictamentre igual
    != Distinto
    !== Distinto estricto
    > Mayor que
    < Menor que
    >= Mayor igual que
    <= Menor igual que
Cuando se comparan valores de distinto tipo, js usa los tipos que tengan en
comun para hacer la comparacion
*/
let resultado = 2 < 4;
console.log(resultado);

let edad = 15;

let result = edad > 18;
console.log(result);

let edad_e = "18";

let result_edad = edad_e == 18;
console.log(result_edad);

let result_edad_e = edad_e === 18;
console.log(result_edad_e);

/*
Operadores logicos
    && AND
    !! Or
    ! Not negacion
    ?? nullish coalescing-> fusion de nulos o union de nulos
    retorna el primer valor falsy o el ultimo valor Truthy
*/

console.log(true && true); // true
console.log(true && false); // false
console.log(false && true); // false
console.log(false && false); // false

console.log(10 && 0); // 0
console.log(10 && 20); // 20

//__ ?? __
// evalua el lado izquierdo y si es nulo o undefined retorna el valor de la derecha si no el de la izquierdas
console.log("" ?? "Hola");
console.log(null ?? "Hola");


//Condiciones
if(10 > 2)
    console.log("10 es mayor");
else {
    console.log("10 es menor");
}

let calificacion = 9;

if(calificacion == 10)
    console.log("EXCELENTE");
else if(calificacion > 7)
    console.log("MUY BIEN");
else if(calificacion > 5)
    console.log("PUEDES MEJORAR");
else
    console.log("REPROBADO");

// CICLOS
// imprimir numeros de 1 al 10
// break termina el ciclo
// continue se salta el bloque

for (let i = 1; i <= 10; i++)
    console.log(i);

let i = 1;
while(i <= 10){
    console.log(i);
    i++;
}

let j = 1;
do{
    console.log(j);
    j++;
}while(j <= 10);
