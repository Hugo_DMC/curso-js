// Declaracion de funcion, se puede declarar despues
saludar();

function saludar(){
  /* codigo de la funcion*/
  console.log("Hola developers");
}

function cuadreado(numero){
  return numero * numero;
}

let cuadrado_numero = cuadreado(5);
console.log(cuadrado_numero);

// expresion de funcion, el nombre de la funcion es opcional, se manda a llamar
// despues de la funcion
let func = function saluda(){}

// scope global y local

var nombre = "Hugo";
console.log(nombre);

function hola(){
    console.log("Hola "+ nombre);
    // si no se le agrega var o let afectara no solo en la funcion sino de manera global
    nombre = "Jompy";
    console.log("Hola "+ nombre);
}

// Alcance de funcion y de bloque
// let y const tienen alcance dentro del bloque mas proximo
// var dentro de la fucnion mas proxima, disponible para toda la funcion
function holaMundo(nombre){
    //Cualquier variable declarada esta disponible dentro del bloque
    if(nombre){
        var saludo = "Hola " + nombre;
    }

    console.log(saludo);
}

holaMundo("Hugo");

/*
Argumentos de funciones
num es el parametro,
-- no tienen un tipo definido
-- no se revisa la cantidad de argumentos enviados,
cuando un parametro no es enviado recibe undefined
asignar un valor por defecto con = en los parametro
se deben de colocar los valores por defecto al final de los parametros que no tengas un valor  por default
*/
function cuadradoNum(num){
    return num * num;
}

// 10 es el argumento, argumentos llenan los parametros
cuadradoNum(10);

// arguments para obtener todos los argumentos enviados
function sumaTodos(){
    console.log(arguments);
    let suma = 0;
    for (let i = 0; i < arguments.length; i++){
        suma += arguments[i];
    }
    console.log(suma);
}
 sumaTodos(1,2,3,4);


/*
pase por valor o por referencia
*/

let e = 29;

function modificar(e){
    e = 25;
    console.log("Dentro d ela funcion: " + e);
}
console.log(e);
// envio de argumento por valor se esta enviando una copia
modificar(e);
console.log(e);

// envio poor referencia, en caso de arreglos y objetos
let a = [29];

function modificarEdad(a){
    a[0] = 25;
    console.log("Dentro d ela funcion: " + a);
}
console.log(a);
modificarEdad(a);
console.log(a);

/*
Funciones puras: nom produce efectos secundarios
*/

let arreglo = [1,2,3];
areglo[0] = 2; // mutacion

let valor = 2;
valor += 1; // mutacion


let edades = [29];

function modificarEdades(edades){
    let copia = [...edades]; // se genera una copia
    copia[0] = 25; // Se modifica la copia y no el valor original
    return copia; // para comunicar los cambios con el exterior
    console.log("Dentro d ela funcion: " + a);
}
console.log(edades);
modificarEdades(edades);
console.log(edades);


/*
First class objects
    Debe ser posible retornarlo
    Debe ser posible ser asignado a una variable
    Debe ser posible enviarlo como argumento

Numbers, string, boolean, funciones y objects son de primera clases
*/
function executor(funcion){
    funcion();
}

function decirHola(){
    console.log("HOLA");
}
//Delegar la ejecucion de una funcion al trabajo de otra
executor(decirHola);


//funcion retornando otra
function build(){
    function a(){
        console.log("HOLA");
    }
    return a;
}

let f = build();

f();

// funcion anonima
function build1(){
    return function a(){console.log("HOLA");
    }
}

let f1 = build1();

f1();


/*
Hoisting - solo con var y fucniones,
las funciones se mueven al principio del alcance
solo en la declaracione de variables y no es la modificacion
*/

console.log(x);
var x;

console.log(suma(10, 10));

function suma(a,b){
    return a + b;
}
