let objeto = {
    func: function(){
        console.log(this);
    }
}

let func = objeto.func;

/*
- Arrow functions, se crea un retorno implicito
- Tienen una sintaxis mas corta que la declaracion con functionsHeredan el
valor de this del contexto en el que fueron creadas y no nse reasigna
()=> en la creacion
function en la ejecucion asigna el contexto
*/
let demo = ()=>{
    console.log("Hola Mundo");
}

let suma = (a,b)=> a+b;
console.log(suma(2,3));

let estudiante = {
    nombre: "Hugo",
    saludar: () => {console.log("hola soy " + this.nombre);},
    saludarAlt: function(){console.log("Hola soy :" +this.nombre);}
}

estudiante.saludar(); // Hola soy undefined error
estudiante.saludarAlt(); // Hola soy hugo

function Estudiante() {
    this.nombre = "Hugo",
    this.saludar = () => {console.log("hola soy " + this.nombre);}
}

let e = new Estudiante();
e.saludar();

setTimeout(e.saludar,100);


// bind, call  y apply

function Estudiante1() {
    this.nombre = "Hugo",
    this.saludar = () => {console.log(this);},
    this.saludar = this.saludar.bind(this);
}

let e1 = new Estudiante1();
e1.saludar();

/*
    Inmediata: call y apply
    Lazy: bind
*/
e1.saludar.call({});
e1.saludar.apply({},["1","2"]);

let nuevaFuncion = e1.saludar.bind({});
nuevaFuncion();
