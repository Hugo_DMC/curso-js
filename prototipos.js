// prototipo
function Course(){}

console.log(Course.prototype);

Course.prototype.inscribir = function(){
    console.log("HOLA");
}

let course = new Course();
course.title = "Hola";
course.inscribir();
console.log(course);

function Curso(){}

Curso.prototype.inscribir = function(){
    console.log("Ya llegué");
}

let js = new Curso();
let ruby = new Curso();

js.inscribir();
ruby.inscribir();


// herrencia de prototipo entre objetos
function Course1(title){ this.title = title;}

let js1 = new Course1("Curso de JS");
let ruby1 = Object.create(js1);

ruby1.title = "Curso de ruby";
console.log(ruby1);
console.log(js1);

/*
En resumen:
    En js la herencia de prototipos funciona al incluir el prototype de una
    clase en la cadena de prototipos de un objeto
    Un objeto puede heredar de otro si lo usamos como primer argumento de Object.create
    Una funcion constructora puede heredar de otra si usamos el prototipo de la
    clase base como primer argumento de Object.create y asignamos ese resultado
    al prototype de la clase hija
*/
function Course2(title){ this.title = title;}
Course2.prototype.inscribir = function(){ console.log("Inscribir");}

function LiveCourse(date){
    this.published_at = date;
}
LiveCourse.prototype.inscribir = Object.create(Course2.prototype); // {__proto__: Course.prototype}

let js2 = new LiveCourse(new Date());
js2.inscribir();
console.log(js2);
